using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdapterDemo
{
    [TestClass]
    public class ReportExporterTest
    {
        [TestMethod]
        public void RenderTwoPatterns()
        {
            var reportExporter = new ReportExporter(new PDFReportAdapter(), new DirectoryUploadAdapter());
            bool result = reportExporter.ExportReport(new Entry());
            Console.Write(result);
            Assert.AreEqual(result, true);
        }
    }
}