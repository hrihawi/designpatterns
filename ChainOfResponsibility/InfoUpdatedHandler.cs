﻿using ApprovalCommon;

namespace ChainOfResponsibilityPattern
{
    interface IInfoUpdatedHandler
    {
        HandleResponse Handle(IEvent expenseReport);
        void RegisterNext(IInfoUpdatedHandler next);
    }

    class InfoUpdatedHandler : IInfoUpdatedHandler
    {
        public InfoUpdatedHandler(IHandle expenseApprover)
        {
            _approver = expenseApprover;
            _next = EndOfChainHandler.Instance;
        }

        public HandleResponse Handle(IEvent expenseReport)
        {
            HandleResponse response = _approver.Handle(expenseReport);

            if (response == HandleResponse.BeyondApprovalLimit)
            {
                return _next.Handle(expenseReport);
            }

            return response;
        }

        public void RegisterNext(IInfoUpdatedHandler next)
        {
            _next = next;
        }

        private readonly IHandle _approver;
        private IInfoUpdatedHandler _next;
    }
}
