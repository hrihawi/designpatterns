﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompositePattern
{
    public class Person : IParticipator
    {
        public string Name { get; set; }
        public int Gold { get; set; }
        
        public void PrintMessage()
        {
            Console.WriteLine("{0} has {1} gold coins.", Name, Gold);
        }
    }
}
