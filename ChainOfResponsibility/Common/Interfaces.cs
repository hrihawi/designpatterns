﻿using System;

namespace ApprovalCommon
{
    public interface IEvent
    {
        Decimal Total { get; }
    }

    public interface IHandle
    {
        HandleResponse Handle(IEvent eventInfo);
    }

    public enum HandleResponse
    {
        Denied,
        Approved,
        BeyondApprovalLimit,
    }
}
