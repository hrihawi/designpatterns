Adapter Pattern: consists of
- Interface that has spesific actions to do (IReportAdapter)
- Deferent immplementations of the previous interface (PDFReportAdapter, CSVReportAdapter, ...)
- Class that is specialized of unique job (DataRender, ReportExporter, ...) 
  * Accept the IReportAdapter interface in the constructor
  * Define list of puplic actions that cover the interface actions



    #region ReportAdapter

    public interface IReportAdapter
    {
        byte[] Render(object data);
    }

    public class PDFReportAdapter : IReportAdapter
    {
        private object _data;
        public byte[] Render(object data)
        {
            _data = data;
            // Render the data into a pdf report as byte[]
            return (byte[])data;
        }
    }

    #endregion

    #region UploadAdapter

    public interface IUploadAdapter
    {
        bool Upload(byte[] data);
    }

    public class DirectoryUploadAdapter : IUploadAdapter
    {
        private object _data;
        public bool Upload(byte[] data)
        {
            _data = data;
            // write the data to system directory
            return true;
        }
    }

    #endregion



    public class ReportExporter
    {
        private readonly IReportAdapter _reportAdapter;
        private readonly IUploadAdapter _uploadAdapter;

        public ReportExporter(IReportAdapter reportAdapter, IUploadAdapter uploadAdapter)
        {
            _reportAdapter = reportAdapter;
            _uploadAdapter = uploadAdapter;
        }

        public bool ExportReport(object data)
        {
            var renderedReport = _reportAdapter.Render(data);
            _uploadAdapter.Upload(renderedReport);
            return true;
        }
    }

    public class Main
    {
        public void Run()
        {
            var reportExporter = new ReportExporter(new PDFReportAdapter(), new DirectoryUploadAdapter());
            reportExporter.ExportReport(new object());
        }
    }