﻿namespace AdapterDemo
{
    public class DirectoryUploadAdapter : IUploadAdapter
    {
        private object _data;
        public bool Upload(byte[] data)
        {
            _data = data;
            // write the data to system directory
            return true;
        }
    }
}
