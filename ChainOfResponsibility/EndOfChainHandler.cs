﻿using System;
using ApprovalCommon;

namespace ChainOfResponsibilityPattern
{
    class EndOfChainHandler : IInfoUpdatedHandler
    {
        private EndOfChainHandler() { }

        public static EndOfChainHandler Instance
        {
            get { return _instance; }
        }

        public HandleResponse Handle(IEvent expenseReport)
        {
            return HandleResponse.Denied;
        }

        public void RegisterNext(IInfoUpdatedHandler next)
        {
            throw new InvalidOperationException("End of chain handler must be the end of the chain!");
        }

        private static readonly EndOfChainHandler _instance = new EndOfChainHandler();
    }
}
