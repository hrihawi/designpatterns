﻿namespace AdapterDemo
{
    public interface IReportAdapter
    {
        byte[] Render(Entry entry);
    }
}
