﻿namespace AdapterDemo
{
    public class ReportExporter
    {
        private readonly IReportAdapter _reportAdapter;
        private readonly IUploadAdapter _uploadAdapter;

        public ReportExporter(IReportAdapter reportAdapter, IUploadAdapter uploadAdapter)
        {
            _reportAdapter = reportAdapter;
            _uploadAdapter = uploadAdapter;
        }

        public bool ExportReport(Entry data)
        {
            var renderedReport = _reportAdapter.Render(data);
            return _uploadAdapter.Upload(renderedReport);
        }
    }
}
