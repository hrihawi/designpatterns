namespace CompositePattern
{
    public interface IParticipator
    {
        int Gold { get; set; }
        void PrintMessage();
    }
}