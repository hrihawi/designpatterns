﻿using System;

namespace BuilderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var sandwich1 = Sandwich.Create(new MySandwichBuilder());
            sandwich1.Display();

            var sandwich2 = Sandwich.Create(new ClubSandwichBuilder());
            sandwich2.Display();

            Console.ReadKey();
        }
    }
}
