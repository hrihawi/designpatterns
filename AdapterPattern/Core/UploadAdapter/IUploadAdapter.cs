﻿namespace AdapterDemo
{
    public interface IUploadAdapter
    {
        bool Upload(byte[] data);
    }
}
