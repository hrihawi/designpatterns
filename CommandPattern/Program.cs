﻿using CommandPattern.Commands;
using System;
using System.Collections.Generic;

namespace CommandPattern
{
    public class Program
    {
        static void Main(string[] args)
        {
            var availableCommands = GetAvailableCommands();

            if (args.Length == 0)
            {
                PrintUsage(availableCommands);
                Console.WriteLine("please type name of command");
                var name = Console.ReadLine();
                Console.WriteLine("please type the value of command");
                var val = Console.ReadLine();
                args = new string[] { name, val };
            }

            var parser = new CommandParser(availableCommands);
            var command = parser.ParseCommand(args);

            command.Execute();
            Console.ReadLine();
        }

        static IEnumerable<ICommandFactory> GetAvailableCommands()
        {
            return new ICommandFactory[]
                {
                    new CreateOrderCommand(),
                    new UpdateQuantityCommand(),
                    new ShipOrderCommand(),
                };
        }

        private static void PrintUsage(IEnumerable<ICommandFactory> availableCommands)
        {
            Console.WriteLine("Usage: LoggingDemo CommandName Arguments");
            Console.WriteLine("Commands:");
            foreach (var command in availableCommands)
                Console.WriteLine("  {0}", command.Description);
        }
    }
}
