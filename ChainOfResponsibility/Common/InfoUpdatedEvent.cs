﻿using System;

namespace ApprovalCommon
{
    public class InfoUpdatedEvent : IEvent
    {
        public InfoUpdatedEvent(decimal total)
        {
            Total = total;
        }

        public decimal Total 
        { 
            get;
            private set;
        }
    }
}
