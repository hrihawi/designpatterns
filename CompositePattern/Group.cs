﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompositePattern
{
    public class Group : IParticipator
    {
        public string Name { get; set; }
        public List<IParticipator> Members { get; set; }

        public Group()
        {
            Members = new List<IParticipator>();
        }

        public int Gold
        {
            get
            {
                return Members.Sum(x => x.Gold);
            }
            set
            {
                var eachSplit = value / Members.Count;
                // left over give it to the first member!
                var leftOver = value % Members.Count;
                foreach (var member in Members)
                {
                    member.Gold += eachSplit + leftOver;
                    leftOver = 0;
                }
            }
        }

        public void PrintMessage()
        {
            foreach (var member in Members)
            {
                member.PrintMessage();
            }
        }
    }
}
