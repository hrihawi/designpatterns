﻿namespace AdapterDemo
{
    public class PDFReportAdapter : IReportAdapter
    {
        private Entry _entry;
        public byte[] Render(Entry entry)
        {
            _entry = entry;
            // Render the data into a pdf report as byte[]
            return new byte[0];
        }
    }
}
