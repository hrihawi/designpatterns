﻿using System;
using ApprovalCommon;

namespace ChainOfResponsibilityPattern
{
    class MainApp
    {
        static void Main()
        {
            InfoUpdatedHandler william = new InfoUpdatedHandler(new UIElement("William Worker", 0));
            InfoUpdatedHandler mary = new InfoUpdatedHandler(new UIElement("Mary Manager", 1000));
            InfoUpdatedHandler victor = new InfoUpdatedHandler(new UIElement("Victor Vicepres", 5000));
            InfoUpdatedHandler paula = new InfoUpdatedHandler(new UIElement("Paula President", 20000));

            william.RegisterNext(mary);
            mary.RegisterNext(victor);
            victor.RegisterNext(paula);

            Decimal expenseReportAmount;
            if (ConsoleInput.TryReadDecimal("Expense report amount:", out expenseReportAmount))
            {
                HandleResponse response = william.Handle(new InfoUpdatedEvent(expenseReportAmount));

                Console.WriteLine("The request was {0}.", response);
                Console.ReadKey();
            }
        }
    }
}
