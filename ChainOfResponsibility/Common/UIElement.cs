﻿using System;

namespace ApprovalCommon
{
    public class UIElement : IHandle
    {
        public UIElement(string name, Decimal approvalLimit)
        {
            Name = name;
            _approvalLimit = approvalLimit;
        }

        public string Name { get; private set; }

        public HandleResponse Handle(IEvent expenseReport)
        {
            return expenseReport.Total > _approvalLimit 
                    ? HandleResponse.BeyondApprovalLimit 
                    : HandleResponse.Approved;
        }

        private readonly Decimal _approvalLimit;
    }
}
